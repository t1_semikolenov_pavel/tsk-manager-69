package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class IncorrectStatusException extends AbstractException {

    public IncorrectStatusException(final String value) {
        super("Error! Incorrect status. Value `" + value + "` not found...");
    }

}
