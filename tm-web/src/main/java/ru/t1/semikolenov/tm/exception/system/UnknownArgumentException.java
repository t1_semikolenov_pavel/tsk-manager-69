package ru.t1.semikolenov.tm.exception.system;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not supported...");
    }

    public UnknownArgumentException(final String argument) {
        super("Error! Argument `" + argument + "` not supported...");
    }

}
