package ru.t1.semikolenov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.dto.request.ProjectShowByIdRequest;
import ru.t1.semikolenov.tm.dto.response.ProjectShowByIdResponse;
import ru.t1.semikolenov.tm.event.ConsoleEvent;
import ru.t1.semikolenov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.semikolenov.tm.dto.model.ProjectDTO;
import ru.t1.semikolenov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken(), id);
        @NotNull final ProjectShowByIdResponse response = getProjectEndpoint().showProjectById(request);
        @Nullable final ProjectDTO project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}